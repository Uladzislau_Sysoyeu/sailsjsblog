/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var passwordHash = require('password-hash');
var bcrypt = require('bcrypt-nodejs');
var Promise = require('bluebird');
var bcryptPromise = Promise.promisifyAll(require('bcrypt-nodejs'));

module.exports = {
  
  schema: true,

  attributes: {
    
    username: {
      type: 'string',
      required: true,
      unique: true
    },
    
    encryptedPassword: {
      type: 'string'
    },
    
    admin: {
      type: 'boolean',
      defaultsTo: false
    },
    
    toJSON: function() {
      var element = this.toObject();
      delete element.encryptedPassword;
      return element;
    }
    
  },
  
  beforeCreate: function(values, next) {
    bcryptPromise
      .genSaltAsync(10)
      .then(function(salt) {
        sails.log.info(salt);
        return bcryptPromise.hashAsync(values.password, salt, null);
      })
      .then(function (hash) {
        values.encryptedPassword = hash;
        return next();
      })
      .catch(function(error) {
        sails.log.error(error);
        return next(error);
      }); 
  },
  
  comparePasswordPromise: function (password, user) {
    var self = this;
    
    return new Promise(function(resolve, reject) {
      bcrypt.compare(password, user.encryptedPassword, function (err, match) {
        if (err) {
          reject(err);
        }
        if (!match) {
          reject();
        }
        resolve();
      });
    });
  },
  
};
