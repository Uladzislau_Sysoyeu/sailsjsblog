/**
 * Place.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var Promise = require('bluebird');

module.exports = {

  attributes: {
    location: 'json',
//    latitude: {
//      type: 'float',
//      required: 'true',
//    },
//    
//    longitude: {
//      type: 'float',
//      required: 'true'
//    },
    
    title: {
      type: 'string',
      required: 'true'
    },
    
    description: {
      type: 'string',
      required: 'true'
    },
    
    picture: {
      type: 'string',
      required: 'true'
    }
  },
  
  near: function (lat, long, radius) {
    var self = this;

    return new Promise(function(resolve, reject) {
      self.native(function (err, collection) {
        if (err) {
          reject(err);
        }
        var query = {
          location: {
            $nearSphere: {
              $geometry: {
                type: 'Point',
                coordinates: [+long, +lat]
              },
              $maxDistance: +radius
            }
          }
        };

        collection.find(query).toArray(function(err, results) {
          if (err) return reject(err);
//          sails.log('Near by: ' + results);
          return resolve(results);
        });
      });
    });
  }
};
