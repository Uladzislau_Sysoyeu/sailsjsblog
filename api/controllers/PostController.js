/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
  create: function (req, res) {
    Post
      .create(req.body)
      .then(function (post) {
        return res.json({ post: post });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },
  
  update: function (req, res) {
    var Id = req.param('id');
    
    var elem = {
      title: req.param('title'),
      description: req.param('description'),
      content: req.param('content')
    };
    
    Post
      .update(Id, elem)
      .then(function (elem) {
        return res.json({ elem: elem });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
    
  },
  
  delete: function (req, res) {
    var Id = req.param('id');
    Post
      .destroy(Id)
      .then(function(posts) {
        return res.json({ posts: posts });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },
  
  index: function (req, res) {
    Post
      .find()
      .sort('id DESC')
      .limit(5)
      .then(function(posts) {
        return res.json({ posts: posts });
      });
  },
  
  page: function (req, res) {
    var page = req.param('page');
    Post
      .find()
      .sort('id DESC')
      .paginate({
        page: page,
        limit: 5
      })
      .then(function (posts) {
        return res.json({ posts: posts });
      });
  }
  
};

