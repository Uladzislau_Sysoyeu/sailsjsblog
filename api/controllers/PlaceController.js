/**
 * PlaceController
 *
 * @description :: Server-side logic for managing places
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create: function (req, res) {
    Place
      .create(req.body)
      .then(function (place) {
        return res.json({ place: place });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },

  delete: function (req, res) {
    var Id = req.param('id');
    Place
      .destroy(Id)
      .then(function(places) {
        return res.json({ places: places });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },

  show: function(req, res) {
    var Id = req.param('id');
    sails.log(Id);
    Place
      .findOne(Id)
      .then(function(place) {
        return res.json( {place: place} );
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },
  
  index: function (req, res) {
    Place
      .find()
      .sort('id DESC')
      .limit(5)
      .then(function(places) {
        return res.json({ places: places });
      });
  },
  
  findPlace: function(req, res) {
    var sendLatitude = req.param('lat');
    var sendLongitude = req.param('long');
    var sendRadius = req.param('radius') || 50000;
//    sails.log(sendLatitude + ' ' + sendLongitude + ' ' + sendRadius);
    
    Place
      .near(sendLatitude, sendLongitude, sendRadius)
      .then(function(places) {
        return res.json({ places: places });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  }
};
