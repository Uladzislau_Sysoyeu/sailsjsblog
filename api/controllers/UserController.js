/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var kue = require('kue');
var jobs = kue.createQueue();

module.exports = {
	create: function(req, res) {
    User
      .create(req.body)
      .then(function(user) {
        return res.json({ user: user, token: jwToken.issue({id: user.id}) });
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },
  
  show: function(req, res) {
    var Id = req.param('id');
    User
      .findOne(Id)
      .then(function(user) {
      return res.json( {user: user} );
      })
      .catch(function(error) {
        return res.json(error.status || 500, { error: error.message });
      });
  },
   
  index: function(req, res) {
    User
      .find()
      .sort('id DESC')
      .then(function(users) {
        return res.json({ users: users });
      });
  },
  
  send: function(req, res) {
    var transporter = nodemailer.createTransport(
      smtpTransport({
        service: 'gmail', 
        auth: {
          user: 'vladsysoev@gmail.com',
          pass: '2011533patriot'
        }
      })
    );
    
    var mail = {
      from: '"Mr.Mrmrmrmr" <mrmr@mrmr.com>',
      to: 'vladsysoev@gmail.com',
      subject: 'test mail',
      text: 'please, dont answer on this mail. Its test'
    };
    
    transporter.sendMail(mail, function(error, info) {
      if(error) {
        return sails.log(error);
      }
      sails.log('Message sent: ' + info.response);
    });
  },
  
  sendkue: function(req, res) {
    var job = jobs.create('email', {
      title: 'This is test message',
      to: 'vladislav.sysoev@effective-soft.com',
      template: 'test-email'
    });

    job
      .on('complete', function(result) {
        sails.log('Message send ', result);
      })
      .on('failed', function(errorMessage) {
        sails.log('Message did not send ', errorMessage);
      });

    job.save(function(err) {
      if(!err) sails.log(job.id, job.data);
    });
  }
  
};

