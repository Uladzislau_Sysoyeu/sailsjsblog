module.exports = {
  index: function (req, res) {
    var username = req.param('username');
    var password = req.param('password');

    if (!username || !password) {
      return res.json(401, {err: 'email and password required'});
    }

    User
      .findOne({ username: username })
      .then(function(user) {
        return User.comparePasswordPromise(password, user);
      })
      .then(function(user, token) {
        return res.json({ user: user, token: jwToken.issue({id : user}) });
      })
      .catch(function (error) {
        return res.json(error.status || 500, { error: error.message });
      });
    }
};